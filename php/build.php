<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	/*
	 * Build all applicable animations
	 */
	include_once("require_settings.php");
	include_once("database.php");

	$itemTypes = $db->query("SELECT * FROM item_type");
	while($type = $itemTypes->fetchArray()) {
		$buildFunction = $type["build_function"];
		$plugin = $type["plugin_id"];
		include_once($SETTINGS["pluginDirectory"].$plugin."/".$buildFunction);
		$itemTypeId = $type["id"];
		build($itemTypeId, $db);
	}