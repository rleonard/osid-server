<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	include_once("require_settings.php");
	/**
	 * Provides a formatted table of the different types of items that a user could create.
	 * Takes a database query object (SQLite3) containing the item types,
	 * Outputs HTML (Bootstrapped) for the table rows.
	*/
	function itemTypeTable($items) {
		$table = <<<EOD
<div class="itemTypeTable">

EOD;
		while($type = $items->fetchArray()) {
			$id = $type["id"];
			$name = $type["name"];
			$displayName = $type["display_name"];
			$description = $type["discription"];
			$screenshots = json_decode($type["screenshot_json"]); // [{"src":"/[...].png","title":"Ooh look!","caption":"We can do cool stuff!"}, ...]
			$carouselIndicators = "";
			$carouselItems = "";
			$screenshotNumber = 0;
			foreach ($screenshots as $screenshot) {
				$class = $screenshotNumber === 0 ? 'class="active"' : '';
				$carouselIndicators .= <<<EOD
				<li data-target="itemTypeTable-{$name}-carousel" data-slide-to="$screenshotNumber"{$class}></li>
EOD;
				$class = $screenshotNumber === 0 ? ' active' : '';
				$src = $screenshot["src"];
				$title = $screenshot["title"];
				$caption = $screenshot["caption"];
				$carouselItems .= <<<EOD
				<div class="item{$class}">
					<img src="{$src}" />
					<div class="carousel-caption">
						<h4>{$title}</h4>
						<p>{$caption}</p>
					</div>
				</div>
EOD;
				$screenshotNumber++;
			}
			$table .= <<<EOD
	<div class"row">
		<div id="itemTypeTable-{$name}-carousel" class="col-md-4 carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				{$carouselIndicators}
			</ol>
			<div class="carousel-inner">
				{$carouselItems}
			</div>
			<a class="left carousel-control" href="#itemTypeTable-{$name}-carousel" data-slide="prev">
				<span class="fa fa-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#itemTypeTable-{$name}-carousel" data-slide="next">
				<span class="fa fa-chevron-right"></span>
			</a>
		</div>
		<div id="itemTypeTable-{$name}-description" class="col-md-8">
			<h3>{$displayName}</h3>
			<p>{$description}</p>
			<div>
				<a class="btn btn-primary" href="new/?id={$id}">Create Entry</a>
			</div>
		</div>
	</div>
EOD;
		}
		$table .= <<<EOD
</div>
EOD;
		return $table;
	}