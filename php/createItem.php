<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	function createItem($type, $SETTINGS, $db, $userInfo) {
		$id = $type["id"];
		$plugin = $type["plugin_id"];
		$file = $type["create_function"];
		include_once($SETTINGS["pluginDirectory"].$plugin."/".$file);
		date_default_timezone_set($SETTINGS["phpTimezone"]);
		$fields = array_merge(
			array(
			"item_type_id"=>$id,
			"create_time"=>time(),
			"approved"=>"false"
			),
			$userInfo,
			create($SETTINGS)
		);
		$fieldNames = "(";
		$fieldValues = "(";
		foreach ($fields as $key => $value) {
			$fieldNames .= ($fieldNames === "(" ? "" : ", ").$key;
			$val = $db->escapeString($value);
			$fieldValues .= ($fieldValues === "(" ? "" : ", ")."'$val'";
		}
		$fieldNames .= ")";
		$fieldValues .= ")";
		$db->exec("INSERT INTO item_entries {$fieldNames} VALUES {$fieldValues}");
	}