<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	include_once("require_settings.php");

	if(!file_exists($SETTINGS["databaseLocation"])) {
		$db = new SQLite3($SETTINGS["databaseLocation"]);
		$db->exec("CREATE TABLE admins (id INTEGER PRIMARY KEY, username, email, password, allow_modification)");
		$db->exec("CREATE TABLE teachers (id INTEGER PRIMARY KEY, username, email, password)");
		$db->exec("CREATE TABLE item_type (id INTEGER PRIMARY KEY, plugin_id, name, display_name, discription, allow_public, screenshot_json, form_file, create_function, build_function, param_object)");
		$db->exec("CREATE TABLE item_entries (id INTEGER PRIMARY KEY, item_type_id, name, description, creator_type, creator_id, param_object, creator_name, create_time, approved, denied, start_time, target_time)");
		$user = $db->escapeString($SETTINGS['initialUsername']);
		$email = $db->escapeString($SETTINGS['initialEmail']);
		$pass = $db->escapeString($SETTINGS['initialPassword']);
		$db->exec("INSERT INTO admins (username, email, password, allow_modification) VALUES ('$user', '$email', '$pass', 'true')");
	}
	else {
		$db = new SQLite3($SETTINGS["databaseLocation"]);
	}