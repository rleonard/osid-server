<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	if(file_exists("../../settings.php")) {
		$errorTitle = "Unauthorized Action";
		$errorMessage = <<<EOD
				Sorry, a settings file already exists (<code>settings.php</code>).
				To edit the file using the web interface, please manually remove the file from the server.
				You can also edit the file manually on the server.
EOD;
	}
	if(!isset($_POST) || !isset($_POST["databaseLocation"])) {
		$errorTitle = "Data Error";
		$errorMessage = <<<EOD
				Sorry, no data was submitted.
				Please <a href="./">try again</a>.
EOD;
	}
	if(isset($errorMessage) || isset($errorTitle)) {
		echo <<<EOD
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Installation | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
		<link href="/css/home.css" rel="stylesheet" />
	</head>
	<body style="background-color: grey">
		<div style="margin-top: 20%; color: white; display: block; position: absolute; width: 60%; left: 20%">
			<h3 style="text-align: center;"><strong>{$errorTitle}</strong></h3>
			<p class="lead" style="color: white; text-align: justify">
				{$errorMessage}
			</p>
		</div>
	</body>
</html>
EOD;
		exit;
	}

	$file = <<<EOD
<?php
	/*
	 * settings.php
	 * Main configuration file for the digital publishing server.
	 * This file was automatically created with public_html/install/install.php
	 * Please make sure you know what you are doing before editing this file.
	*/
	\$SETTINGS = array();

EOD;
	
	$databaseLocation = $_POST["databaseLocation"];
	$file .= <<<EOD

	/*
	 * The database location defines where the database will
	 * be created and accessed from.
	 * The directory that the file is in must already exist,
	 * and must be writable by the web server.
	*/
	\$SETTINGS["databaseLocation"] = "{$databaseLocation}";

EOD;
	
	$initialUsername = $_POST["initialUsername"];
	$file .= <<<EOD

	/*
	 * The initial username will be used to create the first
	 * administrative user, and the account is granted full privilages.
	 * The username can contain any characters.
	*/
	\$SETTINGS["initialUsername"] = "{$initialUsername}";

EOD;
	
	$initialEmail = $_POST["initialEmail"];
	$file .= <<<EOD

	/*
	 * The initial email is used when creating the first administrative
	 * account, and can be used for authentication.
	*/
	\$SETTINGS["initialEmail"] = "{$initialEmail}";

EOD;
	
	$initialPassword = crypt($_POST["initialPassword"]);
	$file .= <<<EOD

	/*
	 * The initial password is used for the first administrative account.
	 * The password must be hashed (using crypt()) when stored in the settings.
	 * Please do not store any password using plaintext inside this file
	 * unless in a testing scenario using an installation that will never
	 * be transitioned to a live location.
	*/
	\$SETTINGS["initialPassword"] = '$initialPassword';

EOD;

	$pluginDirectory = $_POST["pluginDirectory"];
	$file .= <<<EOD

	/*
	 * Plugins are stored in the plugin directory.
	 * The variable must contain the path to an existing
	 * directory that is writable by the web server,
	 * ending in a trailing slash.
	*/
	\$SETTINGS["pluginDirectory"] = "$pluginDirectory";

EOD;
	
	$pluginDirectoryArchive = $_POST["pluginDirectoryArchive"];
	$file .= <<<EOD

	/*
	 * Plugin archives are temporarily stored in the plugin
	 * archive directory after being uploaded.
	 * The variable must contain the path to an existing
	 * directory that is writable by the web server,
	 * ending in a trailing slash.
	*/
	\$SETTINGS["pluginDirectoryArchive"] = "$pluginDirectoryArchive";

EOD;

	$webRoot = $_POST["webRoot"];
	$file .= <<<EOD

	/*
	 * The root URL for the application install.  Used to redirect users to the software.
	 * Should be valid for any users, not just a local address that might be used for the install.
	*/
	\$SETTINGS["webRoot"] = "$webRoot";

EOD;

	$phpTimezone = $_POST["phpTimezone"];
	$file .= <<<EOD

	/*
	 * The timezone is required to correctly display information.
	 * Please enter one of the timezones supported by PHP.
	 * http://php.net/manual/en/timezones.php
	*/
	\$SETTINGS["phpTimezone"] = "$phpTimezone";

EOD;
	
	file_put_contents("../../settings.php", $file);
