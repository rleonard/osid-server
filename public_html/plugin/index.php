<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../";
	include_once($root."php/require_settings.php");
	$installedFile = $SETTINGS["pluginDirectory"]."installed.json";
	/*
	 * Selecting a specific PHP file:
	 * /plugin/?plugin=<plugin domain>&file=<file>
	 * /plugin/index.php?plugin=<plugin domain>&file=<file>
	 *
	 * Selecting a catch-all URL (redirects to routing.php)
	 * Query string formats (the third is prefered):
	 * /plugin/?plugin=<plugin domain>&url=<file>
	 * /plugin/index.php?plugin=<plugin domain>&url=<file>
	 * /plugin/?/<plugin domain>/<url>
	*/
	if(substr($_SERVER['QUERY_STRING'], 0, 1) === "/") {
		preg_match("@^/([^/]+)/(.*)$@", $_SERVER["QUERY_STRING"], $matches);
		$plugin = $matches[1];
		$routing = $SETTINGS["pluginDirectory"].$plugin."/routing.php";
		if(file_exists($routing)) {
			include($routing);
		}
		else {
			die("Routing file not found.");
		}
	}
	if(isset($_GET["url"]) && isset($_GET["plugin"])) {
		$plugin = $_GET["plugin"];
		if(strstr($plugin, "..")) {
			die("Invalid plugin.");
		}
		$routing = $SETTINGS["pluginDirectory"].$plugin."/routing.php";
		if(file_exists($routing)) {
			include($routing);
		}
		else {
			die("Routing file not found.");
		}
	}

	if(isset($_GET["plugin"]) && isset($_GET["file"])) {
		$plugin = $_GET["plugin"];
		$file = $_GET["file"];
		$include = $SETTINGS["pluginDirectory"].$plugin . "/" . $file;
		if(strstr($include, "..")) {
			die("Invalid plugin or file.");
		}
		if(file_exists($include)) {
			if(end(array_values(explode(".", $include))) === "html") {
				echo file_get_contents($include);
			}
			else {
				include($include);
			}
			exit;
		}
	}

	die("File not found.");