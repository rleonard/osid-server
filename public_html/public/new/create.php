<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	if(!isset($_GET) || !isset($_GET["id"])) {
		header("Location: ../");
		exit;
	}
	$root = "../../../";
	include_once($root."php/database.php");
	$id = $db->escapeString($_GET["id"]);
	$item = $db->querySingle("SELECT * FROM item_type WHERE id='$id' LIMIT 1", true);
	if(!$item || !$item["allow_public"]) {
		header("Location: ../?error=notfound");
		exit;
	}
	$root = "../../../";
	include_once($root."php/createItem.php");
	$name = isset($_POST["public_name"]) && $_POST["public_name"] != "" ? $_POST["public_name"]." (unverified)" : "No name entered";
	createItem($item, $SETTINGS, $db, array(
		"creator_type"=>"Public",
		//"creator_id"=>"null",
		"creator_name"=>$name
		)
	);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Entry Submitted | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
		<link href="/css/home.css" rel="stylesheet" />
	</head>
	<body style="background-color: grey">
		<div style="margin-top: 20%; color: white; display: block; position: absolute; width: 60%; left: 20%">
			<h3 style="text-align: center;"><strong>Item Entry Submitted</strong></h3>
			<p class="lead" style="color: white; text-align: justify">
				Your entry will need to be accepted by the administration before it is displayed,
				and will automatically be added to the video rotation once it has been accepted.
			</p>
			<div class="text-centered">
				<a href="../../../" class="btn btn-default">Back</a>
			</div>
		</div>
	</body>
</html>