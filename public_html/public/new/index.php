<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	if(!isset($_GET) || !isset($_GET["id"])) {
		header("Location: ../");
		exit;
	}
	$root = "../../../";
	include_once($root."php/database.php");
	$id = $db->escapeString($_GET["id"]);
	$item = $db->querySingle("SELECT * FROM item_type WHERE id='$id' LIMIT 1", true);
	if(!$item) {
		header("Location: ../?error=notfound");
		exit;
	}
	$scriptAppend = "";
	$formFile = $SETTINGS["pluginDirectory"].$item["plugin_id"]."/".$item["form_file"];
	if(end(array_values(explode(".", $formFile))) === "html") {
		$form = file_get_contents($formFile);
	}
	else {
		include($formFile);
	}

	echo <<<EOD
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Create Entry | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
		<link href="/css/home.css" rel="stylesheet" />
	</head>
	<body>
		<form class="container" role="form" action="create.php?id={$id}" method="POST">
			<div class="form-group">
				<label for="public_name">Full Name</label>
				<input type="text" class="form-control" id="public_name" name="public_name" placeholder="John Doe" />
				<div class="help-block">
					Please enter your full name in case there are any questions about the item entry.
				</div>
			</div>
			{$form}
			<a href="../" class="btn btn-default">Back</a> 
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		<!--<script src="/lib/bootstrap-datepicker.js"></script>-->
		{$scriptAppend}
	</body>
</html>
EOD;
