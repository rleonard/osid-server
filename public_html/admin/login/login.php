<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../../";
	include($root."php/database.php");
	session_start();
	if(!isset($_POST) || !isset($_POST["username"])) {
		header("Location: ./");
		exit;
	}
	$username = $db->escapeString($_POST["username"]);
	$password = $_POST["password"];
	$user = $db->querySingle("SELECT * FROM admins WHERE LOWER(username)=LOWER('$username') OR LOWER(email)=LOWER('$username') LIMIT 1", true);
	if(crypt($password, $user["password"]) !== $user["password"]) {
		//header("Location: ./?invalid=true");
		exit;
	}
	$_SESSION["adminLoggedIn"] = true;
	$_SESSION["user"] = json_encode($user);
	header("Location: ../");
	exit;