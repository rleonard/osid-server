<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Administrator Login | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
	</head>
	<body style="background-color: lightGrey">
		<div style="margin-top: 20%; display: block; position: absolute; width: 60%; left: 20%">
			<h3 style="text-align: center;"><strong>Login</strong></h3>
			<p class="lead" style="color: white; text-align: justify">
				<form role="form" action="login.php" method="POST">
					<?php
						if(isset($_GET["invalid"])) {
							echo <<<EOD
					<p class="text-danger text-center">
						Sorry, your authentication details were incorrect. 
						Please try again.
					</p>
EOD;
						}
					?>
					<div class="form-group">
						<label for="username" class="sr-only">Username or Email Address</label>
						<input type="text" class="form-control" id="username" name="username" placeholder="Username or Email" />
					</div>
					<div class="form-group">
						<label for="password" class="sr-only">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Password" />
					</div>
					<div class="text-center">
						<a href="../../" class="btn btn-default">Cancel</a>
						<button type="submit" class="btn btn-success">Login</button>
					</div>
				</form>
			</p>
		</div>
	</body>
</html>