<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Administration | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
		<link href="/css/home.css" rel="stylesheet" />
	</head>
	<body>
		<div class="row" style="margin-top: 10px;">
			<a href="/admin/approve/" class="col-md-offset-1 col-md-4 well">
				<h3><i class="fa fa-legal"></i> Accept Messages</h3>
				<p>Allow or deny messages to be displayed on the boards.</p>
			</a>
			<a href="/admin/create/" class="col-md-offset-1 col-md-4 well">
				<h3><i class="fa fa-plus"></i> Create a Message</h3>
				<p>Create your own message to be displayed on the boards.</p>
			</a>
		</div>
		<div class="row">
			<a href="/admin/plugins/install/" class="col-md-offset-1 col-md-4 well">
				<h3><i class="fa fa-cogs"></i> Install New animation</h3>
				<p>Install new types of animations.</p>
			</a>
			<a href="/admin/plugins/" class="col-md-offset-1 col-md-4 well">
				<h3><i class="fa fa-wrench"></i> Configure animations</h3>
				<p>Configure the types of animations to be displayed.</p>
			</a>
		</div>
		<div class="row">
			<!--<a href="/admin/plugins/install/" class="col-md-offset-1 col-md-4 well">
				<h3><i class="fa fa-cogs"></i> Install New animation</h3>
				<p>Install new types of animations.</p>
			</a>-->
			<a href="/admin/plugins/" class="col-md-offset-6 col-md-4 well">
				<h3><i class="fa fa-power-off"></i> Logout</h3>
				<p>For security, log out of your account after each session.</p>
			</a>
		</div>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>