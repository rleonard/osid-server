<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	global $root;
	$root = "../../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
	include_once($root."php/database.php");
	include_once($root."php/require_settings.php");
	date_default_timezone_set($SETTINGS["phpTimezone"]);

	if(isset($_GET) && isset($_GET["approve"])) {
		$id = $_GET["approve"];
		$set = "approved='true'";
		$after = function($id, $db, $SETTINGS) {
			global $root;
			$itemType = $db->querySingle("SELECT item_type.plugin_id, item_type.build_function FROM item_type, item_entries WHERE item_entries.id='$id' AND item_entries.item_type_id=item_type.id", true);
			include($SETTINGS["pluginDirectory"].$itemType["plugin_id"].'/'.$itemType["build_function"]);
			build($db, $SETTINGS, "item_approved", $id);
		};
	}
	if(isset($_GET) && isset($_GET["deny"])) {
		$id = $_GET["deny"];
		$set = "denied='true'";
	}
	if(isset($_GET) && isset($_GET["edit"])) {
		$id = $_GET["edit"];
		$name = $db->escapeString($_POST["name"]);
		$description = $db->escapeString($_POST["description"]);
		$set = "approved='true', name='$name', description='$description'";
	}
	if(isset($set)) {
		$id = $db->escapeString($id);
		$db->exec("UPDATE item_entries SET {$set} WHERE id='$id' LIMIT 1");
		if(isset($after)) {
			$after($id, $db, $SETTINGS);
		}
	}

	$page = isset($_GET["page"]) ? $_GET["page"] : 0;
	$perPage = 15;
	$offset = $page * $perPage;
	$entries = $db->query("SELECT * FROM item_entries WHERE approved='false' AND denied IS NOT 'false' AND denied IS NOT 'true' ORDER BY create_time ASC LIMIT $perPage OFFSET $offset");
	$list = "";
	while ($item = $entries->fetchArray()) {
		$entryId = $item["id"];
		$itemName = $item["name"];
		$creatorName = $item["creator_name"];
		$description = $item["description"];
		$date = date("c", $item["target_time"]);
		$englishDate = date("r", $item["target_time"]);
		$dateCreated = date("c", $item["create_time"]);
		$englishDateCreated = date("r", $item["create_time"]);
		$startDate = date("c", $item["start_time"]);
		$startDateEnglish = date("r", $item["start_time"]);
		$time = $item["create_time"];
		$list .= <<<EOD

			<form role="form" action="?page={$page}&perPage={$perPage}&edit={$entryId}" method="POST">
				<label for="{$entryId}name">Name</label>
				<input type="text" class="form-control" id="{$entryId}name" name="name" value="{$itemName}" placeholder="Name" />
				<p class="text-muted">From: {$creatorName}</p>
				<label for="{$entryId}description">Description</label>
				<p>
					<textarea class="form-control" rows="4" id="{$entryId}description" name="description" placeholder="Event description">{$description}</textarea>
				</p>
				<p>
					<span class="fa fa-calendar"></span>
					Starts
					<abbr class="timeago" title="{$startDate}">{$englishDateCreated}</abbr> -
					Expires 
					<abbr class="timeago" title="{$date}">{$englishDate}</abbr><br />
					<span class="fa fa-plus"></span>
					Created
					<abbr class="timeago" title="{$dateCreated}">{$englishDateCreated}</abbr>
				</p>
				<p>
					<a href="?page={$page}&perPage={$perPage}&approve={$entryId}" class="btn btn-success">
						<span class="fa fa-thumbs-up"></span>
						Approve
					</a>
					<a href="?page={$page}&perPage={$perPage}&deny={$entryId}" class="btn btn-danger">
						<span class="fa fa-thumbs-down"></span>
						Deny
					</a>
					<button type="submit" class="btn btn-primary">
						<span class="fa fa-pencil"></span>
						Approve With Changes
					</button>
				</p>
			</form>

EOD;
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Approve Messages | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
		<link href="/css/home.css" rel="stylesheet" />
	</head>
	<body>
		<div class="container">
			<h2><span class="fa fa-legal"></span> Approve Messages</h2>
			<div>
				<a href="../" class="btn btn-default">Back to Administration Panel</a>
			</div>
			<p>
				<?php
					echo $list;
					$nextPage = $page + 1;
					$prevPage = $page - 1;
					if($page > 0) {
						echo <<<EOD
			<a href="?page={$prevPage}&perPage={$perPage}" class="btn btn-default">Previous Page</a>
EOD;
					}
					echo <<<EOD
			<a href="?page={$nextPage}&perPage={$perPage}" class="btn btn-default">Next Page</a>
EOD;
				?>
			</p>
		</div>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		<script src="/lib/timeago.js"></script>
		<script>
			jQuery.timeago.settings.allowFuture = true;
			jQuery(document).ready(function() {
				jQuery("abbr.timeago").timeago();
			});
		</script>
	</body>
</html>