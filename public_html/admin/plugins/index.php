<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
	include_once($root."php/require_settings.php");
	$installedFile = $SETTINGS["pluginDirectory"]."installed.json";
	if(file_exists($installedFile)) {
		$installedPlugins = json_decode(file_get_contents($installedFile), true);
	}
	else {
		$installedPlugins = array();
	}
	$handle = opendir($SETTINGS["pluginDirectory"]);
	$PLUGINS = array();
	while (false !== ($entry = readdir($handle))) {
        if(substr($entry, 0, 1) !== "." && is_dir($SETTINGS["pluginDirectory"].$entry)) {
        	include($SETTINGS["pluginDirectory"].$entry."/conf.php");
        }
    }

    $list = <<<EOD
    	<div id="pluginList" class="container">
EOD;
    foreach ($PLUGINS as $plugin) {
    	$id = $plugin["id"];
    	$buttons = "<div class='pull-right'>";
    	if(!isset($installedPlugins[$id]) || !$installedPlugins[$id]) {
    		$buttons .= <<<EOD
<a class="btn btn-primary" href="enable.php?plugin={$id}">Enable</a>
EOD;
    	}
    	else {
    		if($plugin["config"]) {
    			$buttons .= <<<EOD
<a class="btn btn-success" href="configure.php?plugin={$id}"> <i class="fa fa-cogs">Configure</i></a> 
EOD;
    		}
    		$buttons .= <<<EOD
<a class="btn btn-warning" href="disable.php?plugin={$id}">Disable</a>
EOD;
    	}
    	$buttons .= "</div>";
    	$name = $plugin["name"];
    	$description = $plugin["description"];
    	$screenshotNumber = 0;
    	$carouselIndicators = "";
    	$carouselItems = "";
    	foreach($plugin["screenshots"] as $screenshot) {
    		$class = $screenshotNumber === 0 ? 'class="active"' : '';
			$carouselIndicators .= <<<EOD
				<li data-target="pluginList-{$id}-carousel" data-slide-to="$screenshotNumber"{$class}></li>
EOD;
			$class = $screenshotNumber === 0 ? ' active' : '';
			$src = $screenshot["file"];
			$title = $screenshot["title"];
			$caption = $screenshot["caption"];
			$carouselItems .= <<<EOD
				<div class="item{$class}">
					<img src="/php/get_plugin_image?id={$id}&file={$src}" />
					<div class="carousel-caption">
						<h4>{$title}</h4>
						<p>{$caption}</p>
					</div>
				</div>
EOD;
			$screenshotNumber++;
		}
		$list .= <<<EOD
			<div class="row">
				<div id="pluginList-{$id}-carousel" class="col-md-4 carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						{$carouselIndicators}
					</ol>
					<div class="carousel-inner">
						{$carouselItems}
					</div>
					<a class="left carousel-control" href="#pluginList-{$id}-carousel" data-slide="prev">
						<span class="fa fa-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#pluginList-{$id}-carousel" data-slide="next">
						<span class="fa fa-chrevron-right"></span>
					</a>
				</div>
				<div id="pluginList-{$id}-description" class="col-md-8">
					<h3>{$name}</h3>
					<p>{$description}</p>
					<span class="text-muted">{$id}</span>
					{$buttons}
				</div>
			</div>
EOD;
    }

    $list .= <<<EOD
    	</div>
EOD;
	
	echo <<<EOD
<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Add Plugin | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
	</head>
	<body>
		{$list}
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>
EOD;
