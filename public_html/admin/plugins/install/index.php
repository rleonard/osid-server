<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Add Plugin | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
	</head>
	<body>
		<div class="container">
			<h2>Install animation Plugin</h2>
			<form role="form" action="upload.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="archive">Plugin Archive</label>
					<input type="file" id="archive" name="archive" />
					<p class="help-block">
						Upload a file to add a new animation type
					</p>
				</div>
				<a href="../../" class="btn btn-default">Back</a>
				<button type="submit" class="btn btn-primary">Upload</button>
			</form>
		</div>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>