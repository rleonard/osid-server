<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
	include_once($root."php/require_settings.php");

	function rm_rf($dir) {
		foreach(scandir($dir) as $file) {
			if ('.' === $file || '..' === $file) continue;
			if (is_dir("$dir/$file")) rm_folder_recursively("$dir/$file");
			else unlink("$dir/$file");
		}
		rmdir($dir);
		return true;
	}
	$uploadedFile = $SETTINGS["pluginDirectoryArchive"] . basename($_FILES['archive']['name']);
	if(!move_uploaded_file($_FILES['archive']['tmp_name'], $uploadedFile)) {
		header("Location: ./?errorUpload=true");
		exit;
	}
	$zip = new ZipArchive();
	if(!$zip->open($uploadedFile)) {
		header("Location: ./?errorZip=true");
		exit;
	}
	$zip->extractTo($SETTINGS["pluginDirectory"]);
	$zip->close();
	unlink($uploadedFile);
	header("Location: ../");
	exit;