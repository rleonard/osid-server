<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
	include_once($root."php/require_settings.php");
	include_once($root."php/database.php");
	$plugin = $_GET["plugin"];
	$plugin_id = $db->escapeString($plugin);
	$pluginDirectory = $SETTINGS["pluginDirectory"].$plugin_id."/";
	$PLUGINS = array();
	include($pluginDirectory."conf.php");

	if(isset($_GET["update"])) {
		$saveFile = $PLUGINS[$plugin]["configSave"];
		include($saveFile);
		header("Location: ?plugin={$plugin}&updated=true");
		exit;
	}

	$encloseForm = true;
	$bodyScripts = "";

	$configFile = $PLUGINS[$plugin]["config"];
	$configFilePath = $pluginDirectory.$configFile;
	if(end(array_values(explode(".", $configFile))) === "html") {
		$form = file_get_contents($configFilePath);
	}
	else {
		include($configFilePath);
	}

	$updated = "";
	if(isset($_GET["updated"])) {
		$updated = <<<EOD
<p class="text-success">Settings updated.</p>
EOD;
	}

	if($encloseForm) {
		$inlineForm = <<<EOD
		<form class="container" role="form" action="configure.php?plugin={$plugin}&update=true" method="POST">
			{$form}
		</form>
EOD;
	}
	else {
		$inlineForm = $form;
	}

	echo <<<EOD
<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Configure Plugin | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
	</head>
	<body>
		<h3>Plugin Configuration</h3>
		{$updated}
		{$inlineForm}
		<div>
			<a class="btn btn-default" href="./">Back</a>
		</div>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		$bodyScripts
	</body>
</html>
EOD;
