<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of OSID Server.
	 *
	 * the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$root = "../../../";
	include_once($root."php/auth.php");
	$user = auth("/admin/login/");
	include_once($root."php/require_settings.php");
	include_once($root."php/database.php");
	$plugin_id = $db->escapeString($_GET["plugin"]);
	$pluginDirectory = $SETTINGS["pluginDirectory"].$plugin_id."/";
	$PLUGINS = array();
	include($pluginDirectory."conf.php");
	$types = $PLUGINS[$_GET["plugin"]]["types"];
	foreach ($types as $id => $itemType) {
		$name = $db->escapeString($itemType["name"]);
		$description = $db->escapeString($itemType["description"]);
		$plugin_id = $db->escapeString($itemType["plugin_id"]);
		$display_name = $db->escapeString($itemType["display_name"]);
		$allow_public = $db->escapeString($itemType["allow_public"]);
		$form_file = $db->escapeString($itemType["form_file"]);
		$create_function = $db->escapeString($itemType["create_function"]);
		$build_function = $db->escapeString($itemType["build_function"]);
		$screenshot_json = $db->escapeString(json_encode($itemType["screenshot_json"]));
		$param_object = $db->escapeString(json_encode($itemType["param_object"]));
		$db->exec("INSERT INTO item_type (name, plugin_id, display_name, allow_public, form_file, create_function, build_function, screenshot_json, param_object) VALUES ('$name', '$plugin_id', '$display_name', '$allow_public', '$form_file', '$create_function', '$build_function', '$screenshot_json', '$param_object')");
	}
	$installedFile = $SETTINGS["pluginDirectory"]."installed.json";
	if(file_exists($installedFile)) {
		$installedPlugins = json_decode(file_get_contents($installedFile), true);
	}
	else {
		$installedPlugins = array();
	}
	$installedPlugins[$plugin_id] = true;
	file_put_contents($installedFile, json_encode($installedPlugins));
	echo <<<EOD
<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Plugin Enabled | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
	</head>
	<body>
		<h3>Plugin Enabled</h3>
		<p>The plugin you selected has been enabled.</p>
		<div>
			<a class="btn btn-default" href="./">Back</a>
		</div>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>
EOD;
