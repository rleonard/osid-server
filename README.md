# OSID Server
Project to manage the communication channels for an organization, originally designed for Groton-Dunstable Regional High School.

## Licencing ##
Project created by [Ryan Leonard](https://ryanleonard.us/).  All code Copyright Ryan Leonard, 2013-2014.  
This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions, please see public_html/licence.html or /licence.html for more details.

## Requirements ##

- Web Server (Apache or nginx should work well)
- PHP
- SQLite3 (with module for PHP)
- Domain or subdomain (not just a directory)

### Optional ###
These items might be appreciated for plugins included with the root installation.

- Synfig (for building vector animations)
- Access to internet accounts to publish information (Twitter, Facebook, Google+, etc.)
- Email access (sending), suggested with SMTP or POP (access to account and server allowance)

## Setup ##
### Files ###
For a clean install, you need a directory to place the OSID Server files in.  
During development on Ubuntu Linux, we used `/var/www/osid`,
which would allow all files to be avalible through `http://localhost/osid/`.
This is not suggested for a live install,
although proper configuration of Apache might be able to avoid most of the security problems.

Any directory that is accessible by the user the web server runs under is allowed.  Under at least Linux and OSX, this also means that all parent directories must also have permissions configured to allow this access.

    cd ~/
    wget [url] osid.zip
    gunzip osid.zip
    mkdir /var/www/osid
    cp osid/* /var/www/osid/

Once the files are moved, the directory `public_html/` must be the web root for a domain, either a full domain (`http://example.com/`) or subdomain (`http://publish.example.com/`).

### Configuration ###
The configuration is stored in a PHP file in the same directory that the root website files are in (the directory containing `php/` and `public_html`).

If you want to run an automatic installation, a PHP file can be manually added, or the program will walk you through the configuration variables if the file doesn't exist.

For most users, at this step you should vist the web files in a web browser, and follow the instructions there.

## Resources ##
[Project Wiki](https://bitbucket.org/rleonard/osid-server/wiki/Home)

[Plugin Tutorial](https://bitbucket.org/rleonard/osid-server/wiki/Plugin%20Tutorial) contains information on how to create a plugin

[Plugin List](https://bitbucket.org/rleonard/osid-server/wiki/Plugin%20List) contains a list of known plugins written for the OSID server.

![gplv3-127x51.png](https://bitbucket.org/repo/8RrE8R/images/662343000-gplv3-127x51.png)